import tensorflow as tf


def square_symmetry(image, symmetry=0):
    """
    Преобразует входной тензор с помощью элемента группы симметрии квадрата
    :param image: входной тензор
    :param symmetry: номер элемента группы симметрии квадрата (от 0 до 7)
    :return: преобразованный тензор
    """
    image_symmetry = tf.image.rot90(image, k=symmetry % 4)
    if symmetry > 3:
        image_symmetry = tf.image.flip_left_right(image_symmetry)
    return image_symmetry


class Preprocessor:
    def __init__(self, shape: tuple, n_class: int):
        """
        :param shape: геометрические размеры окон
        :param n_class: количество классов
        """
        self.shape = list(shape)
        self.n_class = n_class

    def load_and_preprocess_image_and_mask(self, image_path: str,
                                           mask_path: str, symmetry: int) -> tuple:
        """
        Возвращает пару изображение, маска
        :param image_path: путь до изображения
        :param mask_path: путь до маски изображения
        :param symmetry: номер элемента группы симметрии квадрата (от 0 до 7)
        """
        image = tf.io.read_file(image_path)
        image = tf.dtypes.cast(tf.image.decode_png(image, channels=3), tf.float32)
        image = tf.image.resize(image, self.shape)
        # для эффективного transfer learning нормируем к [-1, 1]:
        # https://keras.io/api/applications/mobilenet/#mobilenetv2-function
        image = image / 255.0 * 2.0 - 1.0
        # image /= 255.0  # normalize to [0,1] range
        image = square_symmetry(image, symmetry)

        mask = tf.io.read_file(mask_path)
        mask = tf.image.decode_png(mask, channels=1)
        mask = tf.image.resize(mask, self.shape)
        mask = mask % self.n_class
        mask = square_symmetry(mask, symmetry)

        return image, mask

    def get_dataset(self, image_paths: list, mask_paths: list, symmetry_list: list):
        """
        Получаем Dataset -- заготовку из списка путей до изображений
        и их масок
        :param image_paths: список путей до .png окон
        :param mask_paths: список путей до соответствующих .png масок
        :param symmetry_list: список номеров элементов группы симметрии квадрата (от 0 до 7),
                              которые будут применены к соответствующим окнам и маскам
        """
        ds = tf.data.Dataset.from_tensor_slices((image_paths, mask_paths, symmetry_list))
        return ds.map(self.load_and_preprocess_image_and_mask)
