import numpy as np
import pandas as pd
import tensorflow as tf
import time

from math import ceil
from os import listdir
from os.path import join, isdir
from pathlib import Path

from utils.predict.evaluate_handler import EvaluateHandler
from utils.train.metrics import calculate_result, fast_make_report
from utils.train.models_handler import ModelsHandler
from utils.pkl_utils import read_pickle, write_pickle
from utils.train.preprocessor import Preprocessor
from utils.train.samples_generator import SamplesGenerator, get_image_and_mask_paths

AUTOTUNE = tf.data.experimental.AUTOTUNE
MAX_METRICS_N = 5100
SEEDS = [7, 9, 21, 23, 31, 43, 67, 57]


class PipelineHandler:
    def __init__(self, config: dict, test_metrics=True, make_dataset=True, reuse_generator=False):
        """
        :param config: словарь, содержащий конфигурацию обучения
        :param make_dataset: булевый флаг, указывающий на необходимость генерации выборок на диске
        :param reuse_generator: булевый флаг, указывающий на наличие сериализованного генератора
        """
        self.config = config
        self.test_metrics = test_metrics
        self.make_dataset = make_dataset
        self.reuse_generator = reuse_generator
        self.train_path = join(self.config["data_dir"], self.config["dataset_name"] + "TrainDataset")
        self.valid_path = join(self.config["data_dir"], self.config["dataset_name"] + "ValidDataset")
        self.preprocessor = Preprocessor(self.config["shape"], self.config["n_class"])
        self.models_handler = ModelsHandler()
        self.history = None
        # Проверяем существование директорий с .fits-изображениями и масками
        if not isdir(config["channel_config"]["mask_dir"]):
            raise FileExistsError(f"Directory with mask: {config['channel_config']['mask_dir']} doesn't exists.")
        if not isdir(config["channel_config"]["channels_dir"]):
            raise FileExistsError(f"Directory with images: {config['channel_config']['channels_dir']} doesn't exists.")

    def train(self):
        """
        Создаёт наборы данных, формирует из них tf.data.Dataset и запускает обучение
        """
        # заводим генератор и датасет на диске
        self.generate_png_dataset()

        # создаём обучающий и валидационный tf-датасеты
        print("Create TF input pipeline")
        print(f"self.config['augmentation']: {self.config['augmentation']}")
        train_ds = self._make_tf_datasets(self.train_path, is_train=True, is_augmentation=self.config["augmentation"])
        # валидационный датасет
        valid_ds = self._make_tf_datasets(self.valid_path, is_train=False)

        # обучение модели
        print(f"CNN model {self.config['model_type']} summary:")
        self.models_handler.create_model(self.config)
        self.history = self.models_handler.train_and_save_model(train_ds, valid_ds, self.config)

    def calculate_metrics(self):
        """
        Записывает summary модели и вычисляет метрики
        Вычисляет метрики для валидационой выборки
        """
        # формирование summary
        self._make_summary()
        # вычисление метрик
        self.models_handler.read_model(self.config)

        # сначала для валидационной выборки
        valid_ds = self._make_tf_datasets(self.valid_path, is_train=False)
        print("Calculate metrics for valid_ds")
        n_valid_image = self.generator.get_n_valid_fields()
        valid_report = self._calculate_metrics(valid_ds, self.config["n_valid"] * n_valid_image)

        tsv_name = self.config["model_name"] + "_valid_metrics.tsv"
        valid_report.to_csv(join(self.config["model_dir"], tsv_name), sep="\t", header=True, index=False,
                            encoding="utf-8")
        print(valid_report)

        # теперь рассчитываем для тестовой выборки
        if self.test_metrics and self.config["test_dir"]:
            print(f"Calculate metrics for test dataset: {self.config['test_dir']}")
            evaluate_handler = EvaluateHandler(self.config, self.config["model_path"], verbose=0)
            evaluate_handler.evaluate()

        # теперь для обучающей выборки
        # train_ds = self._make_datasets(self.train_path, is_train=False)
        # print("Calculate metrics for train_ds")
        # train_report = self._calculate_metrics(train_ds, self.config["n_valid"] * n_valid_image)
        #
        # tsv_name = self.config["model_name"] + "_train_metrics.tsv"
        # train_report.to_csv(join(self.config["model_dir"], tsv_name), sep="\t", header=True, index=False,
        #                     encoding="utf-8")
        # print(train_report)

    def generate_png_dataset(self):
        """
        Создаёт генератор и данные на диске, проверяя соответствие конфигурации, данных и прочее
        """
        generator_path = join(self.config["generator_dir"], "generator_" + self.config["dataset_name"] + ".pkl")
        # если есть сериализованный генератор -- считываем его
        if self.reuse_generator:
            print(f"Read generator from {generator_path}")
            self.generator = read_pickle(generator_path)
            if self.generator.shape != tuple(self.config["shape"]):
                raise ValueError(
                    f"generator.shape {self.generator.shape} != config['shape'] {tuple(self.config['shape'])}")
            if self.generator.n_train != self.config["n_train"] or self.generator.n_valid != self.config["n_valid"]:
                raise ValueError(f"generator.n_valid = {self.generator.n_valid}, "
                                 f"generator.n_train = {self.generator.n_train} but "
                                 f"config[n_valid] = {self.config['n_valid']}, "
                                 f"config[n_train] = {self.config['n_train']}")
        else:
            print("Generate samples")
            self.generator = SamplesGenerator(self.config)
            self.generator.generate_corners()
            # для потенциального ускорения сериализуем его в .pkl-файл
            Path(generator_path).parent.mkdir(parents=True, exist_ok=True)
            print(f"Write generator to {generator_path}")
            write_pickle(generator_path, self.generator)
        # создание dataset на диске
        if self.make_dataset:
            print("Creating datasets on disk")
            self.generator.generate_png_datasets(self.train_path, self.valid_path)
        else:
            train_png_window_n = len(listdir(join(self.train_path, "images")))
            valid_png_window_n = len(listdir(join(self.valid_path, "images")))
            n_valid_fields = self.generator.get_n_valid_fields()
            if train_png_window_n != self.config["n_train_fields"] * self.config["n_train"] or \
                    valid_png_window_n != n_valid_fields * self.config["n_valid"]:
                raise RuntimeError("png dataset is inconsistent with config, use '--make_dataset True' option "
                                   "or check configuration")

    def _make_tf_datasets(self, data_path: str, is_train: bool, is_augmentation=False):
        """
        Создаёт выборку по записанным на диске данным
        :param data_path: директория с поддиректориями ./images и ./masks, где содержаться .png изображения и маски
        :param is_augmentation: булевый флаг, если True -- делаем аугментацию с помощью группы симметрий квадрата
        :param is_train: булевый флаг, если True -- возвращаем датасет пригодный для обучения
        :return: tf.data.Dataset
        """
        original_image_paths, original_mask_paths = get_image_and_mask_paths(data_path)
        image_paths = []
        mask_paths = []
        symmetries = []
        if is_augmentation:
            for symmetry in range(8):
                symmetries += [symmetry] * len(original_image_paths)
            for seed in SEEDS:
                i_paths, m_paths = get_image_and_mask_paths(data_path, seed=seed)
                image_paths += i_paths
                mask_paths += m_paths
        else:
            symmetries = [0] * len(original_image_paths)
            image_paths = original_image_paths
            mask_paths = original_mask_paths

        ds = self.preprocessor.get_dataset(image_paths, mask_paths, symmetries)
        if is_train:
            ds = ds.shuffle(buffer_size=min(len(image_paths), self.config["buffer_size"]))
            ds = ds.repeat()
            ds = ds.batch(self.config["batch_size"])
            # `prefetch` позволяет датасету извлекать пакеты в фоновом режиме, во время обучения модели.
            ds = ds.prefetch(buffer_size=AUTOTUNE)
        else:
            ds = ds.batch(self.config["batch_size"])
        return ds

    def _calculate_metrics(self, ds, n_image: int):
        """
        Вычисляет метрики для выборки
        :param ds: tf.data.Dataset для которого рассчитываются метрики
        :param n_image: число изображений в выборке ds
        :return: pandas.df с метриками
        """
        n_image = min(n_image, MAX_METRICS_N)  # чтобы вычисления не были слишком долгими
        n_batch = ceil(n_image / self.config["batch_size"])
        print(f"number of evaluated images: {n_batch * self.config['batch_size']}")
        y_true, y_pred = calculate_result(self.models_handler.model, ds, self.config, n_batch)

        print(f"start report making of ds")
        start_time = time.time()
        report = fast_make_report(y_true, y_pred, self.config["target_names"])
        print(f"report making has been completed in {time.time() - start_time}")
        return report

    def _make_summary(self):
        """
        Создаёт summary по пайплайну обучения и записывает его в .tsv файл [model_dir]/[model_name]_summary.tsv
        """
        model_type = [f"{self.config['model_name']} ({self.config['model_type']})"]
        mode = [self.config["mode"]]
        learning_rate = [self.config["learning_rate"]]
        batch_size = [self.config["batch_size"]]
        scale = [f"{self.config['shape'][0]}x{self.config['shape'][1]} / {self.config['scale']}"]
        augmentation_factor = 8 if self.config["augmentation"] else 1
        train_sample_size = [f"{self.config['n_train_fields']} x {augmentation_factor} x {self.config['n_train']}"]
        epochs = [self.config["epochs"]]
        train_vs_valid_fields = [f"{self.config['n_train_fields']} / {self.config['n_valid_fields']}"]
        best_epoch = [np.argmin(self.history.history["val_loss"]) + 1]
        best_val_loss = [round(min(self.history.history["val_loss"]), 5)]
        buffer_size = [self.config["buffer_size"]]
        weights = self.config["n_class"] * [1]
        if "weights" in self.config:
            weights = self.config["weights"]
        n_class = [f"{self.config['n_class']} / {weights}"]

        summary_df = pd.DataFrame({"model_type": model_type, "mode": mode, "learning_rate": learning_rate,
                                   "batch_size": batch_size, "input_shape / scale": scale,
                                   "train_sample_size": train_sample_size, "epochs": epochs,
                                   "train_vs_valid_fields": train_vs_valid_fields, "best_epoch": best_epoch,
                                   "best_val_loss": best_val_loss, "buffer_size": buffer_size,
                                   "n_class / weights": n_class})

        tsv_path = join(self.config["model_dir"], self.config["model_name"] + "_summary.tsv")
        summary_df.to_csv(tsv_path, sep="\t", header=True, index=False, encoding="utf-8")
