import tensorflow as tf
import time

from pathlib import Path
from tensorflow import keras

from utils.models.mobilenet_unet_models import unet_deep_decoder_model, unet_deep_decoder_with_dropout_model, \
    unet_shallow_decoder_model, unet_deep_decoder_with_bn_model
from utils.models.original_unet_model import original_unet_model, original_unet_with_dropout_model, \
    original_unet_with_bn_model
from utils.models.resnet50v2_unet_models import resnet50v2_unet_deep_decoder_model, \
    resnet50v2_unet_deep_decoder_with_dropout_model, resnet50v2_unet_deep_decoder_with_bn_model

MOBILENET_TYPES = [unet_deep_decoder_model.__name__, unet_shallow_decoder_model.__name__,
                   unet_deep_decoder_with_dropout_model.__name__]


def _add_weights(weights):
    """
    Функция для создания функции, возвращающей веса для обучения
    """
    def inner(in_image, in_label):
        # The weights for each class, with the constraint that: sum(class_weights) == 1.0
        class_weights = tf.constant(weights)
        class_weights = class_weights / tf.reduce_sum(class_weights)

        # Create an image of `sample_weights` by using the label at each pixel as an
        # index into the `class weights` .
        sample_weights = tf.gather(class_weights, indices=tf.cast(in_label, tf.int32))

        return in_image, in_label, sample_weights
    return inner


class ModelsHandler:
    """
    Осуществляет регистрацию моделей их инициализацию и обучение
    """

    def __init__(self):
        """
        Производит запись в словарь всех моделей из пакета ./models
        """
        self.model_types = {unet_deep_decoder_model.__name__: unet_deep_decoder_model,
                            unet_shallow_decoder_model.__name__: unet_shallow_decoder_model,
                            unet_deep_decoder_with_dropout_model.__name__: unet_deep_decoder_with_dropout_model,
                            unet_deep_decoder_with_bn_model.__name__: unet_deep_decoder_with_bn_model,
                            original_unet_model.__name__: original_unet_model,
                            original_unet_with_dropout_model.__name__: original_unet_with_dropout_model,
                            original_unet_with_bn_model.__name__: original_unet_with_bn_model,
                            resnet50v2_unet_deep_decoder_model.__name__: resnet50v2_unet_deep_decoder_model,
                            resnet50v2_unet_deep_decoder_with_dropout_model.__name__: resnet50v2_unet_deep_decoder_with_dropout_model,
                            resnet50v2_unet_deep_decoder_with_bn_model.__name__: resnet50v2_unet_deep_decoder_with_bn_model
                            }
        self.model = None

    def create_model(self, config):
        """
        Создаёт модель выбранного типа
        :param config: config для запуска обучения
        """
        if config["model_type"] in self.model_types:
            self.model = self.model_types[config["model_type"]](config)
        else:
            raise ValueError(f"model type {config['model_type']} is not registered")

    def train_and_save_model(self, train_ds, valid_ds, config):
        """
        :param train_ds: tf dataset для обучения модели
        :param valid_ds: tf dataset для контроля за качеством и выбора лучшей модели
        :param config: config для запуска обучения
        :return: история обучения, необходимая для анализа метрик и loss
        """
        # создаём callback для сохранения наилучшей модели по выборке валидации
        model_path = config["model_path"]
        Path(model_path).parent.mkdir(parents=True, exist_ok=True)
        checkpoint = keras.callbacks.ModelCheckpoint(model_path,
                                                     monitor="val_loss",
                                                     # "val_accuracy",
                                                     verbose=1,
                                                     save_best_only=True,
                                                     mode="auto",
                                                     save_freq="epoch")
        n_train = config["n_train"] * config["n_train_fields"]
        if config["augmentation"]:
            n_train *= 8

        device = "/CPU:0"
        if config["GPU"]:
            device = "/device:GPU:0"
        start_time = time.time()
        with tf.device(device):
            if "weights" in config:
                history = self.model.fit(train_ds.map(_add_weights(config["weights"])), epochs=config["epochs"],
                                         verbose=1, steps_per_epoch=n_train // config["batch_size"],
                                         validation_data=valid_ds,
                                         callbacks=[checkpoint])
            else:
                history = self.model.fit(train_ds, epochs=config["epochs"], verbose=1,
                                         steps_per_epoch=n_train // config["batch_size"],
                                         validation_data=valid_ds,
                                         callbacks=[checkpoint])
        print(f"model was trained in {time.time() - start_time} seconds")
        return history

    def read_model(self, config):
        self.model = tf.keras.models.load_model(config["model_path"])
