import datetime
import json
from pathlib import Path

from os import listdir
from os.path import join

KEYS = ["channel_config", "dataset_name", "generator_dir", "data_dir", "n_train_fields", "n_train", "n_valid", "scale",
        "shape", "augmentation", "models_dir", "model_type", "mode", "learning_rate", "target_names", "epochs",
        "batch_size", "buffer_size", "GPU", "test_dir"]
CHANNEL_CONFIG_KEYS = ["mask_dir", "channels_dir", "channels_suffixes"]
STANDARD = "standard"
TRANSFER_LEARNING = "transfer_learning"
FINE_TUNING = "fine_tuning"
MODES = [STANDARD, TRANSFER_LEARNING, FINE_TUNING]


def json_train_config(config_path: str, save_config=True) -> dict:
    """
    Осуществляет обработку .json-конфигурации для обучения модели
    :param config_path: путь до .json файла с конфигурацией
    :param save_config: булевый флаг, если True, то конфигигурация сохраняется
                        в models_dir/model_name/[model_name]_config.json
    :return: преобразованная .json конфигурация
    """
    json_config = read_json(config_path)
    if check_json_config(json_config):
        json_config = save_and_modify_config(json_config, save_config)
    return json_config


def read_json(json_path: str) -> dict:
    try:
        with open(json_path) as json_file:
            json_dict = json.load(json_file)
    except json.JSONDecodeError:
        raise ValueError(f"{json_path} is incorrect .json file")

    return json_dict


def check_json_config(config: dict) -> bool:
    """
    Осуществляет проверку считанной конфигурации на наличие необходимых ключей
    :param config: словарь, содержащий конфигурацию обучения
    """
    for key in KEYS:
        if key not in config:
            raise ValueError(f"Your config doesn't contain '{key}'")

    for key in CHANNEL_CONFIG_KEYS:
        if key not in config["channel_config"]:
            raise ValueError(f"Your channel_config doesn't contain '{key}'")

    if config["mode"] not in MODES:
        raise ValueError(f"Your mode {config['mode']} is incorrect, must be on of these: '{STANDARD}', "
                         f"'{TRANSFER_LEARNING}', '{FINE_TUNING}'.")
    return True


def save_and_modify_config(json_config: dict, save_config=True) -> dict:
    """
    Сохраняет исходную .json конфигурацию в models_dir/model_name/[model_name]_config.json и
    преобразует её, добавляя новые поля "n_valid_fields"; "n_class"; "model_dir", "model_name" и "model_path",
    а также изменяя тип поля "shape"
    :param json_config: сохраняемая .json-конфигурация
    :param save_config: булевый флаг, если True, то конфигигурация сохраняется
                        в models_dir/model_name/[model_name]_config.json
    :return: преобразованная .json конфигурация
    """
    now = datetime.datetime.now()
    model_name = f"{now.year}_{'0' * (now.month < 10)}{now.month}_{'0' * (now.day < 10)}{now.day}_" + \
                 f"{'0' * (now.hour < 10)}{now.hour}_{'0' * (now.minute < 10)}{now.minute}_model"
    model_sub_dir = join(json_config["models_dir"], model_name)

    if save_config:
        Path(model_sub_dir).mkdir(parents=True, exist_ok=True)

        config_path = join(model_sub_dir, model_name + "_config.json")
        with open(config_path, mode="w", encoding="utf8") as writer:
            writer.write(json.dumps(json_config, indent=4, sort_keys=False))

    all_fits_names = [file.split(".")[0] for file in listdir(json_config["channel_config"]["mask_dir"])]
    json_config["n_valid_fields"] = len(all_fits_names) - json_config["n_train_fields"]
    json_config["n_class"] = len(json_config["target_names"])
    json_config["model_dir"] = model_sub_dir
    json_config["model_name"] = model_name
    json_config["model_path"] = join(model_sub_dir, model_name)
    json_config["shape"] = tuple(json_config["shape"])
    return json_config
