import tensorflow as tf

from tensorflow_examples.models.pix2pix import pix2pix
from tensorflow.keras.applications import ResNet50V2
from tensorflow.keras.layers import BatchNormalization, Concatenate, Conv2D, Conv2DTranspose, Input, SpatialDropout2D
from tensorflow.keras.losses import SparseCategoricalCrossentropy
from tensorflow.keras.optimizers import Adam

from utils.train.config_handler import TRANSFER_LEARNING, FINE_TUNING

DROPOUT_PROB = 0.2


def resnet50v2_unet_deep_decoder_model(config):
    """
    Модель UNet
    Encoder: ResNet50V2
    Decoder: Pix2pix + convolution layers
    :param config: config для запуска обучения
    """
    input_shape = tuple(list(config["shape"]) + [3])  # задаём размеры входного 3D тензора
    # Для начала конфигурируем энкодер
    # скачивает веса в /home/alex/.keras/models, в общем случае в $HOME/.keras/models
    if config["mode"] in [TRANSFER_LEARNING, FINE_TUNING]:
        base_model = ResNet50V2(input_shape=input_shape, include_top=False, weights="imagenet")
    else:
        base_model = ResNet50V2(input_shape=input_shape, include_top=False, weights=None)

    # Use the activations of these layers
    layer_names = [
        "conv1_conv",  # 64x64
        "conv2_block3_1_relu",  # 32x32
        "conv3_block4_1_relu",  # 16x16
        "conv4_block6_1_relu",  # 8x8
        "conv5_block3_2_relu",  # 4x4
    ]
    base_model_outputs = [base_model.get_layer(name).output for name in layer_names]

    # Create the feature extraction model
    down_stack = tf.keras.Model(inputs=base_model.input, outputs=base_model_outputs)
    if config["mode"] == TRANSFER_LEARNING:
        down_stack.trainable = False  # делаем freeze, чтобы не обучать эти веса

    # Теперь конфигурируем декодер
    up_stack_n_filters = [512, 256, 128, 64]
    up_stack = [pix2pix.upsample(n_filters, 3) for n_filters in up_stack_n_filters]

    inputs = Input(shape=input_shape)

    # Downsampling through the model
    skips = down_stack(inputs)
    # разворачиваем в обратном порядке, чтобы было соответствие входам декодера
    x = skips[-1]  # 4x4
    skips = reversed(skips[:-1])

    # Upsampling and establishing the skip connections
    for up, n_filters, skip in zip(up_stack, up_stack_n_filters, skips):
        x = up(x)
        x = Concatenate()([x, skip])

        # добавляем конволюционных слоёв, как в оригинальном UNet
        # https://medium.com/geekculture/u-net-implementation-from-scratch-using-tensorflow-b4342266e406
        x = Conv2D(n_filters, 3, activation='relu', padding='same', kernel_initializer='he_normal')(x)
        x = Conv2D(n_filters, 3, activation='relu', padding='same', kernel_initializer='he_normal')(x)

    # This is the last layer of the model
    # слой деконволюции, 3 -- размер фильтра
    last = Conv2DTranspose(config["n_class"], 3, strides=2, padding='same')  # 64x64 -> 128x128
    x = last(x)

    model = tf.keras.Model(inputs=inputs, outputs=x)
    model.summary()

    model.compile(optimizer=Adam(learning_rate=config["learning_rate"]),
                  loss=SparseCategoricalCrossentropy(from_logits=True), metrics=['accuracy'])
    return model


def resnet50v2_unet_deep_decoder_with_dropout_model(config):
    """
    Модель UNet
    Encoder: ResNet50V2
    Decoder: Pix2pix + convolution layers + SpatialDropout2D
    :param config: config для запуска обучения
    """
    input_shape = tuple(list(config["shape"]) + [3])  # задаём размеры входного 3D тензора
    # Для начала конфигурируем энкодер
    # скачивает веса в /home/alex/.keras/models, в общем случае в $HOME/.keras/models
    if config["mode"] in [TRANSFER_LEARNING, FINE_TUNING]:
        base_model = ResNet50V2(input_shape=input_shape, include_top=False, weights="imagenet")
    else:
        base_model = ResNet50V2(input_shape=input_shape, include_top=False, weights=None)

    # Use the activations of these layers
    layer_names = [
        "conv1_conv",  # 64x64
        "conv2_block3_1_relu",  # 32x32
        "conv3_block4_1_relu",  # 16x16
        "conv4_block6_1_relu",  # 8x8
        "conv5_block3_2_relu",  # 4x4
    ]
    base_model_outputs = [base_model.get_layer(name).output for name in layer_names]

    # Create the feature extraction model
    down_stack = tf.keras.Model(inputs=base_model.input, outputs=base_model_outputs)
    if config["mode"] == TRANSFER_LEARNING:
        down_stack.trainable = False  # делаем freeze, чтобы не обучать эти веса

    # Теперь конфигурируем декодер
    up_stack_n_filters = [512, 256, 128, 64]
    up_stack = [pix2pix.upsample(n_filters, 3) for n_filters in up_stack_n_filters]

    inputs = Input(shape=input_shape)

    # Downsampling through the model
    skips = down_stack(inputs)
    # разворачиваем в обратном порядке, чтобы было соответствие входам декодера
    x = skips[-1]  # 4x4
    skips = reversed(skips[:-1])

    # Upsampling and establishing the skip connections
    for up, n_filters, skip in zip(up_stack, up_stack_n_filters, skips):
        x = up(x)
        x = Concatenate()([x, skip])

        # добавляем конволюционных слоёв, как в оригинальном UNet
        # https://medium.com/geekculture/u-net-implementation-from-scratch-using-tensorflow-b4342266e406
        x = Conv2D(n_filters, 3, activation='relu', padding='same', kernel_initializer='HeNormal')(x)
        x = Conv2D(n_filters, 3, activation='relu', padding='same', kernel_initializer='HeNormal')(x)

        x = SpatialDropout2D(DROPOUT_PROB)(x)

    # This is the last layer of the model
    # слой деконволюции, 3 -- размер фильтра
    last = Conv2DTranspose(config["n_class"], 3, strides=2, padding='same')  # 64x64 -> 128x128
    x = last(x)

    model = tf.keras.Model(inputs=inputs, outputs=x)
    model.summary()

    model.compile(optimizer=Adam(learning_rate=config["learning_rate"]),
                  loss=SparseCategoricalCrossentropy(from_logits=True), metrics=['accuracy'])
    return model


def resnet50v2_unet_deep_decoder_with_bn_model(config):
    """
    Модель UNet
    Encoder: ResNet50V2
    Decoder: Pix2pix + convolution layers + BatchNormalization
    :param config: config для запуска обучения
    """
    input_shape = tuple(list(config["shape"]) + [3])  # задаём размеры входного 3D тензора
    # Для начала конфигурируем энкодер
    # скачивает веса в /home/alex/.keras/models, в общем случае в $HOME/.keras/models
    if config["mode"] in [TRANSFER_LEARNING, FINE_TUNING]:
        base_model = ResNet50V2(input_shape=input_shape, include_top=False, weights="imagenet")
    else:
        base_model = ResNet50V2(input_shape=input_shape, include_top=False, weights=None)

    # Use the activations of these layers
    layer_names = [
        "conv1_conv",  # 64x64
        "conv2_block3_1_relu",  # 32x32
        "conv3_block4_1_relu",  # 16x16
        "conv4_block6_1_relu",  # 8x8
        "conv5_block3_2_relu",  # 4x4
    ]
    base_model_outputs = [base_model.get_layer(name).output for name in layer_names]

    # Create the feature extraction model
    down_stack = tf.keras.Model(inputs=base_model.input, outputs=base_model_outputs)
    if config["mode"] == TRANSFER_LEARNING:
        down_stack.trainable = False  # делаем freeze, чтобы не обучать эти веса

    # Теперь конфигурируем декодер
    up_stack_n_filters = [512, 256, 128, 64]
    up_stack = [pix2pix.upsample(n_filters, 3) for n_filters in up_stack_n_filters]

    inputs = Input(shape=input_shape)

    # Downsampling through the model
    skips = down_stack(inputs)
    # разворачиваем в обратном порядке, чтобы было соответствие входам декодера
    x = skips[-1]  # 4x4
    skips = reversed(skips[:-1])

    # Upsampling and establishing the skip connections
    for up, n_filters, skip in zip(up_stack, up_stack_n_filters, skips):
        x = up(x)
        x = Concatenate()([x, skip])

        # добавляем конволюционных слоёв, как в оригинальном UNet
        # https://medium.com/geekculture/u-net-implementation-from-scratch-using-tensorflow-b4342266e406
        x = Conv2D(n_filters, 3, activation='relu', padding='same', kernel_initializer='HeNormal')(x)
        x = BatchNormalization()(x)

        x = Conv2D(n_filters, 3, activation='relu', padding='same', kernel_initializer='HeNormal')(x)
        x = BatchNormalization()(x)

    # This is the last layer of the model
    # слой деконволюции, 3 -- размер фильтра
    last = Conv2DTranspose(config["n_class"], 3, strides=2, padding='same')  # 64x64 -> 128x128
    x = last(x)

    model = tf.keras.Model(inputs=inputs, outputs=x)
    model.summary()

    model.compile(optimizer=Adam(learning_rate=config["learning_rate"]),
                  loss=SparseCategoricalCrossentropy(from_logits=True), metrics=['accuracy'])
    return model
