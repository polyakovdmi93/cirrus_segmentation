import tensorflow as tf

from tensorflow_examples.models.pix2pix import pix2pix
from tensorflow.keras.layers import BatchNormalization, Concatenate, Conv2D, Conv2DTranspose, Input, SpatialDropout2D
from tensorflow.keras.applications import MobileNetV2
from tensorflow.keras.losses import SparseCategoricalCrossentropy
from tensorflow.keras.optimizers import Adam

from utils.train.config_handler import TRANSFER_LEARNING, FINE_TUNING

DROPOUT_PROB = 0.2


def unet_deep_decoder_model(config):
    """
    Модель UNet
    Encoder: MobileNetV2
    Decoder: Pix2pix + convolution layers
    :param config: config для запуска обучения
    """
    input_shape = tuple(list(config["shape"]) + [3])  # задаём размеры входного 3D тензора
    # Для начала конфигурируем энкодер
    # скачивает веса в /home/alex/.keras/models, в общем случае в $HOME/.keras/models
    if config["mode"] in [TRANSFER_LEARNING, FINE_TUNING]:
        base_model = MobileNetV2(input_shape=input_shape, include_top=False, weights="imagenet")
    else:
        base_model = MobileNetV2(input_shape=input_shape, include_top=False, weights=None)
    # Use the activations of these layers
    layer_names = [
        'block_1_expand_relu',  # 64x64
        'block_3_expand_relu',  # 32x32
        'block_6_expand_relu',  # 16x16
        'block_13_expand_relu',  # 8x8
        'block_16_project',  # 4x4
    ]
    base_model_outputs = [base_model.get_layer(name).output for name in layer_names]

    # Create the feature extraction model
    down_stack = tf.keras.Model(inputs=base_model.input, outputs=base_model_outputs)
    if config["mode"] == TRANSFER_LEARNING:
        down_stack.trainable = False  # делаем freeze, чтобы не обучать эти веса

    # Теперь конфигурируем декодер
    up_stack_n_filters = [512, 256, 128, 64]
    up_stack = [pix2pix.upsample(n_filters, 3) for n_filters in up_stack_n_filters]

    inputs = Input(shape=input_shape)

    # Downsampling through the model
    skips = down_stack(inputs)
    # разворачиваем в обратном порядке, чтобы было соответствие входам декодера
    x = skips[-1]  # 4x4
    skips = reversed(skips[:-1])

    # Upsampling and establishing the skip connections
    for up, n_filters, skip in zip(up_stack, up_stack_n_filters, skips):
        x = up(x)
        x = Concatenate()([x, skip])

        # добавляем конволюционных слоёв, как в оригинальном UNet
        # https://medium.com/geekculture/u-net-implementation-from-scratch-using-tensorflow-b4342266e406
        x = Conv2D(n_filters, 3, activation='relu', padding='same', kernel_initializer='HeNormal')(x)
        x = Conv2D(n_filters, 3, activation='relu', padding='same', kernel_initializer='HeNormal')(x)

    # This is the last layer of the model
    # слой деконволюции, 3 -- размер фильтра
    last = Conv2DTranspose(config["n_class"], 3, strides=2, padding='same')  # 64x64 -> 128x128
    x = last(x)

    model = tf.keras.Model(inputs=inputs, outputs=x)
    model.summary()

    model.compile(optimizer=Adam(learning_rate=config["learning_rate"]),
                  loss=SparseCategoricalCrossentropy(from_logits=True), metrics=['accuracy'])
    return model


def unet_deep_decoder_with_bn_model(config):
    """
    Модель UNet
    Encoder: MobileNetV2
    Decoder: Pix2pix + convolution layers + BathNormalization
    :param config: config для запуска обучения
    """
    input_shape = tuple(list(config["shape"]) + [3])  # задаём размеры входного 3D тензора
    # Для начала конфигурируем энкодер
    # скачивает веса в /home/alex/.keras/models, в общем случае в $HOME/.keras/models
    if config["mode"] in [TRANSFER_LEARNING, FINE_TUNING]:
        base_model = MobileNetV2(input_shape=input_shape, include_top=False, weights="imagenet")
    else:
        base_model = MobileNetV2(input_shape=input_shape, include_top=False, weights=None)
    # Use the activations of these layers
    layer_names = [
        'block_1_expand_relu',  # 64x64
        'block_3_expand_relu',  # 32x32
        'block_6_expand_relu',  # 16x16
        'block_13_expand_relu',  # 8x8
        'block_16_project',  # 4x4
    ]
    base_model_outputs = [base_model.get_layer(name).output for name in layer_names]

    # Create the feature extraction model
    down_stack = tf.keras.Model(inputs=base_model.input, outputs=base_model_outputs)
    if config["mode"] == TRANSFER_LEARNING:
        down_stack.trainable = False  # делаем freeze, чтобы не обучать эти веса

    # Теперь конфигурируем декодер
    up_stack_n_filters = [512, 256, 128, 64]
    up_stack = [pix2pix.upsample(n_filters, 3) for n_filters in up_stack_n_filters]

    inputs = Input(shape=input_shape)

    # Downsampling through the model
    skips = down_stack(inputs)
    # разворачиваем в обратном порядке, чтобы было соответствие входам декодера
    x = skips[-1]  # 4x4
    skips = reversed(skips[:-1])

    # Upsampling and establishing the skip connections
    for up, n_filters, skip in zip(up_stack, up_stack_n_filters, skips):
        x = up(x)
        x = Concatenate()([x, skip])

        # добавляем конволюционных слоёв, как в оригинальном UNet
        # https://medium.com/geekculture/u-net-implementation-from-scratch-using-tensorflow-b4342266e406
        x = Conv2D(n_filters, 3, activation='relu', padding='same', kernel_initializer='HeNormal')(x)
        x = BatchNormalization()(x)

        x = Conv2D(n_filters, 3, activation='relu', padding='same', kernel_initializer='HeNormal')(x)
        x = BatchNormalization()(x)

    # This is the last layer of the model
    # слой деконволюции, 3 -- размер фильтра
    last = Conv2DTranspose(config["n_class"], 3, strides=2, padding='same')  # 64x64 -> 128x128
    x = last(x)

    model = tf.keras.Model(inputs=inputs, outputs=x)
    model.summary()

    model.compile(optimizer=Adam(learning_rate=config["learning_rate"]),
                  loss=SparseCategoricalCrossentropy(from_logits=True), metrics=['accuracy'])
    return model


def unet_deep_decoder_with_dropout_model(config):
    """
    Модель UNet
    Encoder: MobileNetV2
    Decoder: Pix2pix + convolution layers + SpatialDropout2D
    :param config: config для запуска обучения
    """
    input_shape = tuple(list(config["shape"]) + [3])  # задаём размеры входного 3D тензора
    # Для начала конфигурируем энкодер
    # скачивает веса в /home/alex/.keras/models, в общем случае в $HOME/.keras/models
    if config["mode"] in [TRANSFER_LEARNING, FINE_TUNING]:
        base_model = MobileNetV2(input_shape=input_shape, include_top=False, weights="imagenet")
    else:
        base_model = MobileNetV2(input_shape=input_shape, include_top=False, weights=None)
    # Use the activations of these layers
    layer_names = [
        'block_1_expand_relu',  # 64x64
        'block_3_expand_relu',  # 32x32
        'block_6_expand_relu',  # 16x16
        'block_13_expand_relu',  # 8x8
        'block_16_project',  # 4x4
    ]
    base_model_outputs = [base_model.get_layer(name).output for name in layer_names]

    # Create the feature extraction model
    down_stack = tf.keras.Model(inputs=base_model.input, outputs=base_model_outputs)
    if config["mode"] == TRANSFER_LEARNING:
        down_stack.trainable = False  # делаем freeze, чтобы не обучать эти веса

    # Теперь конфигурируем декодер
    up_stack_n_filters = [512, 256, 128, 64]
    up_stack = [pix2pix.upsample(n_filters, 3) for n_filters in up_stack_n_filters]

    inputs = tf.keras.layers.Input(shape=input_shape)

    # Downsampling through the model
    skips = down_stack(inputs)
    # разворачиваем в обратном порядке, чтобы было соответствие входам декодера
    x = skips[-1]  # 4x4
    skips = reversed(skips[:-1])

    # Upsampling and establishing the skip connections
    for up, n_filters, skip in zip(up_stack, up_stack_n_filters, skips):
        x = up(x)
        x = Concatenate()([x, skip])

        # добавляем конфолюционных слоёв, как в оригинальном UNet
        # https://medium.com/geekculture/u-net-implementation-from-scratch-using-tensorflow-b4342266e406
        x = Conv2D(n_filters, 3, activation='relu', padding='same', kernel_initializer='HeNormal')(x)
        x = Conv2D(n_filters, 3, activation='relu', padding='same', kernel_initializer='HeNormal')(x)

        x = SpatialDropout2D(DROPOUT_PROB)(x)

    # This is the last layer of the model
    # слой деконволюции, 3 -- размер фильтра
    last = Conv2DTranspose(config["n_class"], 3, strides=2, padding='same')  # 64x64 -> 128x128

    x = last(x)

    model = tf.keras.Model(inputs=inputs, outputs=x)
    model.summary()

    model.compile(optimizer=Adam(learning_rate=config["learning_rate"]),
                  loss=SparseCategoricalCrossentropy(from_logits=True), metrics=['accuracy'])
    return model


def unet_shallow_decoder_model(config):
    """
    Модель UNet
    Encoder: MobileNetV2
    Decoder: Pix2pix
    :param config: config для запуска обучения
    """
    input_shape = tuple(list(config["shape"]) + [3])  # задаём размеры входного 3D тензора
    # Для начала конфигурируем энкодер
    # скачивает веса в /home/alex/.keras/models, в общем случае в $HOME/.keras/models
    if config["mode"] in [TRANSFER_LEARNING, FINE_TUNING]:
        base_model = MobileNetV2(input_shape=input_shape, include_top=False, weights="imagenet")
    else:
        base_model = MobileNetV2(input_shape=input_shape, include_top=False, weights=None)
    # Use the activations of these layers
    layer_names = [
        'block_1_expand_relu',  # 64x64
        'block_3_expand_relu',  # 32x32
        'block_6_expand_relu',  # 16x16
        'block_13_expand_relu',  # 8x8
        'block_16_project',  # 4x4
    ]
    base_model_outputs = [base_model.get_layer(name).output for name in layer_names]

    # Create the feature extraction model
    down_stack = tf.keras.Model(inputs=base_model.input, outputs=base_model_outputs)
    if config["mode"] == TRANSFER_LEARNING:
        down_stack.trainable = False  # делаем freeze, чтобы не обучать эти веса, если используем transfer_learning

    # Теперь конфигурируем декодер
    up_stack = [pix2pix.upsample(n_filters, 3) for n_filters in [512, 256, 128, 64]]

    inputs = Input(shape=input_shape)

    # Downsampling through the model
    skips = down_stack(inputs)
    # разворачиваем в обратном порядке, чтобы было соответствие входам декодера
    x = skips[-1]  # 4x4
    skips = reversed(skips[:-1])

    # Upsampling and establishing the skip connections
    for up, skip in zip(up_stack, skips):
        x = up(x)
        x = Concatenate()([x, skip])

    # This is the last layer of the model
    # слой деконволюции, 3 -- размер фильтра
    last = Conv2DTranspose(config["n_class"], 3, strides=2, padding='same')  # 64x64 -> 128x128
    x = last(x)

    model = tf.keras.Model(inputs=inputs, outputs=x)
    model.summary()

    model.compile(optimizer=Adam(learning_rate=config["learning_rate"]),
                  loss=SparseCategoricalCrossentropy(from_logits=True), metrics=['accuracy'])
    return model
