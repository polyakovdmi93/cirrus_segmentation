### Instruction

##### Training model
To train a model, it is required directories with fields in 3 bands, and masks, and .json configuration.
Hereafter we assume that we use g, r, and i bands.  
Training .json configuration:
```json
{
   "channel_config":{
      "mask_dir":"/hdd/Galaxies/Scripts/Data/Cirrus_16_05_21/results",
      "channels_dir":"/hdd/Galaxies/Scripts/Data/Cirrus_16_05_21/data",
      "channels_suffixes":[
         "_g.rec.fits",
         "_r.rec.fits",
         "_i.rec.fits"
      ]
   },
   "dataset_name":"20x500image128",
   "data_dir":"/hdd/Galaxies/Scripts/Cirrus/PNGDataset/ForUNetDatasets",
   "generator_dir":"/hdd/Galaxies/Scripts/Cirrus/UNetSegmentation",
   "n_train_fields":15,
   "n_train":500,
   "n_valid":200,
   "scale": 1,
   "shape":[
      128,
      128
   ],
   "augmentation":true,
   "models_dir":"/hdd/Galaxies/Scripts/Cirrus/UNetSegmentation",
   "model_type": "unet_shallow_decoder_model",
   "mode": "standard",
   "learning_rate": 1.0e-3,
   "target_names": [
      "background",
      "cirrus"
   ],
   "weights": [
      1.0,
      9.0
   ],
   "epochs":3,
   "batch_size":32,
   "buffer_size":1000,
   "GPU":false,
   "test_dir": "/external_data/Data/TestCirrus_23_12_21"
}
```
1. "channel_config" is dictionary with information about data. 
   * "mask_dir" is path to directory with masks. **File names in this directory must look like [field_name].fits**.
   * "channels_dir" is path to directory with fields in **g**, **r**, **i** bands. **File names in this directory must look like [field_name][channel_suffix]**.
   * "channels_suffixes" is list with suffixes of fields name for **g**, **r**, **i** bands.
2. "dataset_name" is name of dataset which will be generated and record to storage before training process starts.
3. "data_dir" is a directory with two subdirectories for storing training and validation samples. 
   config["dataset_name"] + "TrainDataset" и config["dataset_name"] + "ValidDataset" are the subdirectories' names. 
   The subdirectories are created automatically.
4. "generator_dir" is a directory where will be stored generator of train and validation datasets. 
   Generator name is "generator_" + config["dataset_name"] + ".pkl".
5. "n_train_fields" is a number of random chosen fields which will be used for training dataset generation.
   Remaining fields will be used for validation dataset generation.
6. "n_train" is a number of windows cut from one field for training dataset.
7. "n_valid" is a number of windows cut from one field for validation dataset.
8. "scale" is a size ratio between window shape and input tensor spatial shape.
   window_shape = scale * tensor_shape
9. "shape" is a list containing a spatial shape of input tensor. **The shape should be square.**
10. "augmentation" is a boolean flag. If True, cropped windows are augmented using symmetry group of square 
    (this procedure increased the number of windows by 8 times).
11. "models_dir" is a path to directory where will be stored a model, metrics, configuration and summary of the model. 
    Model will be stored in directory with name "[yyyy_mm_dd_hh_mm]_model". Prefix is the date of training process starts.
12. "model_type" is a type of neural network model used. Corresponding function names is registered in **./utils/models_handler.py**.
13. "mode" is a type of training strategy. It is one of "standard", "transfer_learning", "fine_tuning".
14. "learning_rate" is a learning rate passed to models' optimizers.
15. "target_names" is a list containing class names for labels from 0 to (n_class-1) 
    **If number of classes is 2, then modulo 2 will apply to masks.**
16. "weights" is a list containing class weights for labels from 0 to (n_class-1). It is an optional argument. Class weights are not use by default.
17. "epochs" is a number of training epochs.
18. "batch_size" is a batch size.
19. "buffer_size" is a maximum size of buffer used for data shuffling.
20. "GPU" is a boolean flag. If True, GPU is used for network training.
21. "test_dir" -- is a directory containing fields for test. Masks is contained in subdirectory "results",
    Fields in *g*, *r*, *i* bands is contained  in subdirectory "data". If null, the test will not be conducted.

To start training process run command with one positional argument (path to .json configuration) and 3 optional arguments:
```bash
    $ python3.x train_segmentation.py config.json --test_metrics [false|true] --make_dataset [false|true] --reuse_generator [false|true]
```
1. test_metrics is a boolean flag. If True and "test_dir" field in .json configuration isn't null, the test will be conducted. defult=True.
2. make_dataset is a boolean flag. If True, training and validation datasets will be record to storage.
   If False, previously recorded data will be used. default=True.
3. reuse_generator is a boolean flag. If True, dataset generator will be recorded to storage in "generator_dir".
   If True, previously recorded generator will be used. default=False.

##### Inference
Module **predict_fields.py** predicts cirrus for new fields.
All input data must stored in single directory **data_dir**.
Data for each field consists of 3 .fits image for *g*, *r*, *i* bands.
They must be stored by such path **data_dir**/[g|r|i]/[**field_name**]_[g|r|i].rec.fits.

To start prediction, it is required trained model, corresponding training configuration, 
and additional .json configuration which looks like:
```json
{
  "quantile": [
    551,
    947,
    1340
  ],
  "train_config": "/home/dpolyakov/Documents/models/2021_12_14_7_44_model_config.json",
  "model_path": "/home/dpolyakov/Documents/models/2021_12_14_7_44_model",
  "data_dir": "/mnt/data/S82_IAC_full",
  "result_dir": "/mnt/data/neural_cirrus",
  "GPU": false
}
```
1. "quantile" is a list of 0.999-quantiles in $g$, $r$, $i$ bands.
2. "train_config" is a path to the training configuration.
3. "model_path" is a path to the model directory.
4. "data_dir" is a directory containing input data.
5. "result_dir" is a directory where will be stored the result of prediction.
6. "GPU" is a boolean flag. If True, GPU is used for network training.

To predict cirrus run command with one positional argument (path to additional .json configuration):
```bash
    $ python3.x predict_fields.py config.json
```

#### Testing
It is possible to run tests separately of training using **evaluate_test.py** module.
To start testing, it is required training configuration with non-null "test_dir" field.
Obtained metrics will be stored to the parent directory of **model_dir** under the name of [model_name]_real_test_metrics.tsv

To start testing run command with 2 positional arguments (path to training configuration and directory containing tf-model):
```bash
    $ python3.x evaluate_test.py config.json model_dir
```

#### Generator updating
There is a possibility to update datasets' generator using **update_generator.py** module.
При необходимости можно обновить генератор (это может понадобиться при оценки метрик на тестовой выборке
или предсказании масок на новых данных).
To start updating run command with positional argument and optional argument.
```bash
    $ python3.x update_generator.py config.json --make_dataset [false|true]
```
1. config.json is a path to the training .json configuration.
2. make_dataset is a boolean flag. If True, training and validation datasets will be record to storage.
   If False, previously recorded data will be used. default=True.