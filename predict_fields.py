import argparse
import numpy as np
import tensorflow as tf

from os import listdir
from os.path import join, basename, exists, isdir
from pathlib import Path

from utils.fits_utils import save_fits
from utils.predict.evaluate_handler import EvaluateHandler, BANDS
from utils.train.config_handler import read_json

KEYS = ["quantile", "train_config", "model_path", "data_dir", "result_dir", "GPU"]


def handle_predict_config(predict_config_path: str) -> dict:
    json_predict_config = read_json(predict_config_path)

    for key in KEYS:
        if key not in json_predict_config:
            raise ValueError(f"Your predict config doesn't contain '{key}'")

    return json_predict_config


class Predictor:
    def __init__(self, predict_config_path: str):
        self.predict_config = handle_predict_config(predict_config_path)
        self.train_config = read_json(self.predict_config["train_config"])
        self.device = "/device:GPU:0" if self.predict_config["GPU"] else "/CPU:0"

        # загружаем модель
        self.model = tf.keras.models.load_model(self.predict_config["model_path"])
        self.model.summary()

        # при необходимости создаём директорию для результатов
        if not exists(self.predict_config["result_dir"]):
            Path(self.predict_config["result_dir"]).mkdir(parents=True, exist_ok=True)

    def _process_field(self, field_name: str):
        """
        Осуществляет предсказание циррусов для поля с именем field_name
        и сохраняет полученную маску в .fits файл
        :param field_name: имя поля, для которого делается предсказание
        """
        # формируем путь до изображений поля
        channel_paths = [join(self.predict_config["data_dir"], field_name, band,
                              f"{field_name}_{band}.rec.fits") for band in BANDS]

        # предсказываем маску для поля
        print(f"Start {field_name} evaluation")
        pred_mask = EvaluateHandler.predict_field(channel_paths, self.predict_config["quantile"],
                                                  self.train_config["shape"], self.train_config["scale"], self.model,
                                                  self.device)

        model_name = basename(self.predict_config["model_path"])
        output_path = join(self.predict_config["result_dir"], model_name + f"_{field_name}.fits")

        # записываем предсказанную маску
        save_fits(pred_mask.astype(np.uint8), output_path)

    def predict(self):
        field_names = [name for name in listdir(self.predict_config["data_dir"])
                       if isdir(join(self.predict_config["data_dir"], name))]
        for field_name, index in zip(field_names, range(len(field_names))):
            if index % 5 == 1:
                print(f"{index} fields have already prepared.")
            self._process_field(field_name)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Script to evaluate cirrus mask for fields in directory")
    parser.add_argument("config", help="Configuration file (.json or .tsv file)", type=str)

    args = parser.parse_args()

    config = args.config

    print(f"CONFIG: {config}")

    predictor = Predictor(config)
    predictor.predict()
