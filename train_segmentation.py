from __future__ import absolute_import, division, print_function, unicode_literals

import argparse

from utils.train.config_handler import json_train_config
from utils.parser_utils import str2bool
from utils.train.pipeline_handler import PipelineHandler


def train_by_json(config: dict, test_metrics=True, make_dataset=True, reuse_generator=False):
    """
    Осуществляет обучение модели сегментации по конфигурации
    :param config: словарь, содержащий конфигурацию обучения
    :param test_metrics: булевый флаг, указывающий на расчёт метрик на тестовой выборке
    :param make_dataset: булевый флаг, указывающий на необходимость генерации обучающей
                         и валидационной .png выборок на диске
    :param reuse_generator: булевый флаг, указывающий на наличие сериализованного генератора
    """
    pipeline_handler = PipelineHandler(config, test_metrics, make_dataset, reuse_generator)
    pipeline_handler.train()
    pipeline_handler.calculate_metrics()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Script to train model for cirrus segmentation")
    parser.add_argument("config", help="Configuration file (.json or .tsv file)", type=str)
    parser.add_argument("--test_metrics", help="Optional: Boolean flag for evaluation metrics on test dataset, "
                                               "default: true",
                        type=str2bool, default=True)
    parser.add_argument("--make_dataset", help="Optional: Boolean flag for writing dataset on disk, default: true",
                        type=str2bool, default=True)
    parser.add_argument("--reuse_generator", help="Optional: Boolean flag for generator reuse, default: false",
                        type=str2bool, default=False)

    args = parser.parse_args()

    config = args.config
    test_metrics = args.test_metrics
    make_dataset = args.make_dataset
    reuse_generator = args.reuse_generator

    print(f"CONFIG: {config}")
    print(f"test_metrics: {test_metrics}")
    print(f"make_dataset: {make_dataset}")
    print(f"reuse_generator: {reuse_generator}")

    train_by_json(json_train_config(config), test_metrics=test_metrics, make_dataset=make_dataset, reuse_generator=reuse_generator)
